<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Prueba PHP 4</title>
</head>
<body>
<?php
	$n1=24.5;
	echo "Valor de la variable n1: ". $n1;
	echo " Tipo de Dato:</b> ". gettype($n1). "<br><br>";
	
	$n1="HOLA";
	echo "Valor de la variable n1: ". $n1;
	echo " Tipo de Dato: ". gettype($n1). "<br><br>";
	
	settype($n1, "integer");
	echo "var_dump(n1): ";
	var_dump($n1);
?>
</body>
</html>
