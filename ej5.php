<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Prueba PHP 5</title>
	<style>
		table, th, td {
		  border: 1px solid black;
		  border-collapse: collapse;
		}
		td {
			text-align: center;
		}
		th {
			background-color: darkgreen;
		}
		.table-result tr:nth-child(even) {
			background-color: lightGrey;
		}
	</style>
</head>
<body>
	<table class="table-result">
		<tr>
			<th>Números</th>
			<th>Resultado</th>
		</tr>
		<?php
			for ($n = 0; $n<= 10; $n++) {
				echo "<tr>";
				echo "<td>".$n." * 9 </td>";
				echo "<td>".$n * 9 ."</td>";
				echo "</tr>";
			}
		?>
	</table>
</body>
</html>