<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Prueba PHP 6</title>
</head>
<body>
	<?php
		$a = rand(99,999);
		$b = rand(99,999);
		$c = rand(99,999);
		echo "Valores de a, b y c = " .$a. ", " .$b. " y " .$c ."<br><br>";
		
		$result = (($a * 3) > ($b + $c)) ? ($a * 3) ." es mayor que ". ($b + $c) : ($b + $c) ." es mayor o igual que ". ($a * 3);
		
		echo $result;
		
	?>
</body>
</html>
